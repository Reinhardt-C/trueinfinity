/*
	  ______                            _ _        _   _                   _    
	 |  ____|                          | | |      | \ | |                 (_)    
	 | |__  __  ___ __   __ _ _ __   __| | |_ __ _|  \| |_   _ _ __ ___    _ ___ 
	 |  __| \ \/ / '_ \ / _` | '_ \ / _` | __/ _` | . ` | | | | '_ ` _ \  | / __|
	 | |____ >  <| |_) | (_| | | | | (_| | || (_| | |\  | |_| | | | | | |_| \__ \
	 |______/_/\_\ .__/ \__,_|_| |_|\__,_|\__\__,_|_| \_|\__,_|_| |_| |_(_) |___/
				 | |                                                     _/ |    
				 |_|                                                    |__/   
	concept by Naruyoko
	most of the code by Naruyoko and copied and adapted for ExpandtaNum.js
	copy and pasting by Reinhardt / Mathemusician / whatever
	
*/
let ExpandtaNum, Decimal, D;
ExpandtaNum = Decimal = D = function(input, layer, sign) {
	return new DecimalClass(input, layer, sign);
}
ExpandtaNum.MSI = 9007199254740991;
ExpandtaNum.ME = Math.log10(9007199254740991);
ExpandtaNum.JSLIMIT = 1.797693134862315e308;
ExpandtaNum.cloneArray = function(array) {
	let a = [...array];
	for (let i in a) {
		if (a[i] instanceof Array) a[i] = ExpandtaNum.cloneArray(a[i]);
	}
	return a;
}
ExpandtaNum.cmp = (x, y) => D(x).cmp(y);
ExpandtaNum.gt = (x, y) => D(x).gt(y);
ExpandtaNum.gte = (x, y) => D(x).get(y);
ExpandtaNum.lt = (x, y) => D(x).lt(y);
ExpandtaNum.lte = (x, y) => D(x).lte(y);
ExpandtaNum.eq = (x, y) => D(x).eq(y);
ExpandtaNum.min = (x, y) => D(x).min(y);
ExpandtaNum.max = (x, y) => D(x).max(y);
ExpandtaNum.add = (x, y) => D(x).add(y);
ExpandtaNum.sub = (x, y) => D(x).sub(y);
ExpandtaNum.mul = (x, y) => D(x).mul(y);
ExpandtaNum.div = (x, y) => D(x).div(y);
ExpandtaNum.pow = (x, y) => D(x).pow(y);
ExpandtaNum.log = (x, y) => D(x).log(y);
ExpandtaNum.floor = (x) => D(x).floor();
ExpandtaNum.abs = (x) => D(x).abs();
ExpandtaNum.neg = (x) => D(x).neg();
ExpandtaNum.rcp = (x) => D(x).rcp();
ExpandtaNum.log10 = (x) => D(x).log10();

class DecimalClass {
	constructor(input, layer, sign) {
		if (typeof input == 'number') {
			if (input == Infinity) this.array = [[input, 0]];
			else if (input < ExpandtaNum.MSI) this.array = [[input, 0]];
			else this.array = [[Math.log10(input), 0], [1, 1]];
			this.layer = layer || 0;
			this.sign = sign || 1;
		} else if (typeof input == 'string') {
			let str = input.replace(',', '');
			if (!isNaN(parseFloat(str))) {
				this.array = [[parseFloat(str), 0]];
			}
			this.layer = 0;
			this.sign = 1;
		} else if (input instanceof Array) {
			this.array = input;
			this.layer = layer || 0;
			this.sign = sign || 1;
		} else if (input instanceof Object) {
			if (input instanceof ExpandtaNum) this.array = ExpandtaNum.cloneArray(input.array);
			else this.array = input.array;
			this.layer = input.layer || layer || 0;
			this.sign = input.sign || sign || 1;
		}
		this.standardize();
	}
	
	// binary operators
	
	add(other) {
		this.standardize();
		other = D(other);
		if (this.max(other).gt([[ExpandtaNum.MSI, 0], [1, 1]])) return this.max(other);
		if (this.sign == -1) return this.neg().add(other.neg()).neg();
		if (other.sign == -1) return this.sub(other);
		if (this.eq(0)) return other;
		if (other.eq(0)) return this;
		if (this.isNaN || other.isNaN || !this.isFinite && !other.isFinite && this.eq(other.neg())) return new OmegaNum(NaN);
		if (!this.isFinite) return this;
		if (!other.isFinite) return other;
		if (this.lt(ExpandtaNum.JSLIMIT) && other.lt(ExpandtaNum.JSLIMIT)) return D(this.toNumber() + other.toNumber());
		let m = this.max(other);
		let a0 = m.array[0] || [0, 0];
		let a1 = m.array[1] || [0, 1];
		let a = a1[0] > 0 ? a0[0] : Math.log10(a0[0]);
		return D([[a + Math.log10(Math.pow(10, this.min(other).array[0][0] - a) + 1), 0], [1, 1]]);
	}
	
	sub(other) {
		this.standardize();
		other = D(other);
		if (this.max(other).gt([[ExpandtaNum.MSI, 0], [1, 1]])) return this.gt(other) ? this.max(other) : this.max(other).neg();
		if (this.sign == -1) return this.neg().sub(other.neg()).neg();
		if (other.sign == -1) return this.add(other);
		if (this.eq(0)) return other.neg();
		if (other.eq(0)) return this;
		if (this.isNaN || other.isNaN || !this.isFinite && !other.isFinite && this.eq(other.neg())) return new OmegaNum(NaN);
		if (!this.isFinite) return this;
		if (!other.isFinite) return other.neg();
		if (this.lt(ExpandtaNum.JSLIMIT) && other.lt(ExpandtaNum.JSLIMIT)) return D(this.toNumber() - other.toNumber());
		let m = this.min(other);
		let a0 = m.array[0] || [0, 0];
		let a1 = m.array[1] || [0, 1];
		let a = a1[0] > 0 ? a0[0] : Math.log10(a0[0]);
		return D([[a + Math.log10(Math.pow(10, this.max(other).array[0][0] - a) - 1), 0], [1, 1]]);
	}
	
	mul(other) {
		this.standardize();
		other = D(other);
		if (this.sign * other.sign == -1) return this.abs().mul(other.abs()).neg();
		if (this.sign == -1) return this.abs().mul(other.abs());
		if (this.isNaN || other.isNaN || this.eq(0) && !other.isFinite || !this.isFinite && other.abs().eq(0)) return new OmegaNum(NaN);
		if (other.eq(0)) return new OmegaNum(0);
		if (other.eq(1)) return this.clone();
		if (!this.isFinite) return this;
		if (!other.isFinite) return other;
		if (this.max(other).gt([[ExpandtaNum.MSI, 0], [2, 1]])) return x.max(other);
		if (this.toNumber() * other.toNumber() < ExpandtaNum.MSI) return this.toNumber() * other.toNumber();
		return ExpandtaNum.pow(10, this.log10().add(other.log10()));
	}
	
	div(other) {
		let x = D(this);
		other = D(other);
		if (x.sign * other.sign==-1) return x.abs().div(other.abs()).neg();
		if (x.sign == -1) return x.abs().div(other.abs());
		if (x.isNaN || other.isNaN || x.isInfinite && other.isInfinite || x.eq(0) && other.eq(0)) return D(NaN);
		if (other.eq(0)) return D(Infinity);
		if (other.eq(1)) return x;
		if (x.eq(other)) return D(1);
		if (!x.isFinite) return x;
		if (!other.isFinite) return D(0);
		if (x.max(other).gt([[ExpandtaNum.MSI, 0], [2, 1]])) return x.gt(other) ? D(x) :D(0);
		let n = x.toNumber() / other.toNumber();
		if (n <= ExpandtaNum.MSI) return D(n);
		let pw = ExpandtaNum.pow(10, x.log10().sub(other.log10()));
		let fp = pw.floor();
		if (pw.sub(fp).lt(D(1e-9))) return fp;
		return pw;
	}
	
	pow(other) {
		this.standardize();
		other = D(other);
		if (other.eq(0)) return D(1);
		if (other.eq(1)) return D(this);
		if (other.lt(0)) return this.pow(other.neg()).rec();
		if (this.lt(0) && other.isInteger){
			if (other.mod(2).lt(1)) return this.abs().pow(other);
			return this.abs().pow(other).neg();
		}
		if (this.lt(0)) return D(NaN);
		if (this.eq(1)) return D(1);
		if (this.eq(0)) return D(0);
		if (this.max(other).gt([[ExpandtaNum.MSI, 0], [1, 2]])) return this.max(other);
		if (this.eq(10)) {
			if (other.gt(0)) {
				if (other.findOperand(1) > -1) {
					other.array[other.findOperand(1)] = (other.array[other.findOperand(1)] + 1);
				} else {
					other.array.push([1, 1]);
				}
				other.standardize();
				return other;
			} else {
				return D(10 ** other.toNumber());
			}
		}
		if (other.lt(1)) return this.root(other.rec());
		let n = this.toNumber() ** other.toNumber();
		if (n <= ExpandtaNum.MSI) return D(n);
		return ExpandtaNum.pow(10, this.log10().mul(other));
	}
	
	root(other) {
		other = D(other);
		if (other.eq(1)) return D(this);
		if (other.lt(0)) return this.root(other.neg()).rec();
		if (other.lt(1)) return this.pow(other.rec());
		if (this.lt(0) && other.isInteger && other.mod(2).eq(1)) return this.neg().root(other).neg();
		if (this.lt(0)) return D(NaN);
		if (this.eq(1)) return D(1);
		if (this.eq(0)) return D(0);
		if (this.max(other).gt([[ExpandtaNum.MSI, 0], [1, 2]])) return this.gt(other) ? D(this) : D(0);
		return ExpandtaNum.pow(10, this.log10().div(other));
	}
	
	log(base) {
		if (base === undefined) base = Math.E;
		return this.log10().div(ExpandtaNum.log10(base));
	}
	
	tetr(other) {
		var t= D(this);
		other = D(other);
        if (!other.isFinite && other.sign>0){
			if (this.gt(Math.pow(Math.E, 1 / Math.E))) return D(Infinity);
			// Formula for infinite height power tower.
			var negln = t.ln().neg();
			return negln.lambertw().div(negln);
		}
		if (other.lte(-2)) return D(NaN);
		if (t.eq(0)){
			if (other.eq(0)) return D(NaN);
			if (other.mod(2).eq(0)) return D(0);
			return D(1);
		}
		if (t.eq(1)){
			if (other.eq(-1)) return D(NaN);
			return D(1);
		}
		if (other.eq(-1)) return D(0);
		if (other.eq(0)) return D(1);
		if (other.eq(1)) return t;
		if (other.eq(2)) return t.pow(t);
		if (t.eq(2)){
			if (other.eq(3)) return D(16);
			if (other.eq(4)) return D(65536);
		}
		if (t.max(other).gt([[ExpandtaNum.MSI, 0], [1, 3]])) return t.max(other);
		let r;
		if (other.gt(ExpandtaNum.MSI)) {
			if (t.gt([[ExpandtaNum.MSI, 0], [1, 2]])){
				r = D(t);
				r.array[t.findOperand(2)]--;
				var j = r.add(other);
				if (j.findOperand(2) != -1) j.array[j.findOperand(2)][0]++
				else j.array.push([1, 2]);
				j.standardize();
				return j;
			}
			if (other.findOperand(2) != -1) other.array[other.findOperand(2)][0]++;
			else other.array.push([1, 2]);
			other.standardize();
			return other;
		}
		let y = other.toNumber();
		let f = Math.floor(y);
		r = ExpandtaNum.pow(10, y-f);
		for (var i = 0; f != 0 && r.lt([[ExpandtaNum.MSI, 0], [1, 1]]) && i < 100; ++i){
			if (f > 0){
				r = t.pow(r);
				--f;
			} else {
				r = r.logBase(t);
			}
		}
		if (i == 100) f = 0;
		if (r.findOperand(1) > -1) {
			r.array[r.findOperand(1)][0] += f;
		} else {
			r.array.push([f, 1]);
		}
		r.standardize();
		return r;
	}
	
	// arrow(arrows) {
		// var t = D(this);
		// arrows = D(arrows);
		// if (!arrows.isInteger || arrows.lt(0)) return function(other) {return D(NaN)};
		// if (arrows.eq(0)) return function(other) {return t.mul(other);};
		// if (arrows.eq(1)) return function(other) {return t.pow(other);};
		// if (arrows.eq(2)) return function(other) {return t.tetr(other);};
		// if (arrows.layer > 1 && t.gt(2)) return function(other) {return D(arrows.array, arrows.layer + 1);
		// return function (other) {
			// other = D(other);
			// if (!other.isInteger || other.lt(0)) return D(NaN);
			// if (other.eq(0)) return D(1);
			// if (other.eq(1)) return D(t);
			// if (other.eq(2)) return t.arrow(arrows-1)(t);
			// if (arrows.lt(ExpandtaNum.MSI) ? t.max(other).gt([[ExpandtaNum.MSI, 0], [1, arrows.toNumber()]) : t.max(other).gt()) return t.max(other);
			// var r;
			// if (other.gt(MAX_SAFE_INTEGER)){
				// if (t.gt("10{"+arrows+"}"+MAX_SAFE_INTEGER)){
					// r=t.clone();
					// r.array[arrows]--;
					// var j=r.add(other);
					// j.array[arrows]=(j.array[arrows]||0)+1;
					// j.standardize();
					// return j;
				// }
				// other.array[arrows]=(other.array[arrows]||0)+1;
				// other.standardize();
				// return other;
			// }
			// r=t.arrow(arrows-1)(t.arrow(arrows-1)(t));
			// r.array[arrows-1]=(r.array[arrows-1]+other.sub(3).toNumber())||other.sub(3).toNumber();
			// r.standardize();
			// return r;
		// }
	// }
  
	
	mod(other) {
		this.standardize();
		other = D(other);
		if (other.eq(0)) return D(0);
		if (this.sign * other.sign < 0) return this.abs().mod(other.abs()).neg();
		if (this.sign < 0) return this.abs().mod(other.abs());
		return this.sub(this.div(other).floor().mul(other));
	}
	
	// unary operators
	
	floor() {
		let x = D(this);
		if (!x.isInteger) x.array[0][0] = Math.floor(x.array[0][0]);
		return x;
	}
	
	abs() {
		let x = D(this);
		x.sign = 1;
		return x;
	}
	
	neg() {
		let x = D(this);
		x.sign *= -1;
		return x;
	}
	
	rec() {
		if (this.isNaN || this.eq(0)) return D(NaN);
		if (this.abs().gt([[308, 0], [1, 1]])) return D(0);
		return D(1 / this.toNumber());
	}
	
	log10() {
		let x = D(this);
		if (x.lt(0)) return D(NaN);
		if (x.eq(0)) return D(-Infinity);
		if (x.lt(ExpandtaNum.MSI)) return D(Math.log10(x.toNumber()));
		
		if (!x.isFinite) return x;
		
		if (x.gt([[ExpandtaNum.MSI, 0], [1, 2]])) return x;
		x.array[x.findOperand(1)][0]--;
		x.standardize();
		return x;
	}
	
	ln() {
		return this.logBase(Math.E);
	}
	
	// comparison operators
	
	cmp(other) {
		let x = D(other);
		let y = D(this);
		if (x.isNaN || y.isNaN) return NaN;
		if (this.layer > x.layer) return 1;
		if (this.layer < x.layer) return -1;
		if (this.highest_operation > x.highest_operation) return 1;
		if (this.highest_operation < x.highest_operation) return -1;
		for (let i = this.array.length - 1; i >= 0; i--) {
			if (!x.array[i]) return 1;
			if (this.array[i][1] > x.array[i][1]) return 1;
			if (this.array[i][1] < x.array[i][1]) return -1;
			if (this.array[i][0] > x.array[i][0]) return 1;
			if (this.array[i][0] < x.array[i][0]) return -1;
		}
		if (x.array.length > this.array.length) return -1;
		return 0;
	}
		
	gt(other) {
		return this.cmp(other) == 1;
	}
	
	gte(other) {
		return this.cmp(other) > -1;
	}
	
	lt(other) {
		return this.cmp(other) == -1;
	}
	
	lte(other) {
		return this.cmp(other) < 1;
	}
	
	gte(other) {
		return this.cmp(other) < 1;
	}
	
	eq(other) {
		return this.cmp(other) == 0;
	}
	
	min(other) {
		if (this.gt(other)) return D(other);
		return D(this);
	}
	
	max(other) {
		if (this.lt(other)) return D(other);
		return D(this);
	}
	
	// test functions
	
	get isFinite() {
		return isFinite(this.array[0][0]);
	}
	
	get isNaN() {
		return isNaN(this.array[0][0]);
	}
	
	get isNotDecimal() {
		return !this.isFinite || !(this instanceof DecimalClass);
	}
	
	get isInteger() {
		if (this.gt(ExpandtaNum.MSI)) return true;
		return this.array[0][0] == Math.floor(this.array[0][0]);
	}
	
	// output functions
	
	toNumber() {
		this.standardize();
		if (this.sign == -1) return this.abs().toNumber() * -1;
		if (this.gt(ExpandtaNum.JSLIMIT)) return Infinity;
		if (this.array.length > 1) return 10 ** this.array[0][0];
		return this.array[0][0];
	}
	
	// standardize tools
	
	get highest_operation() {
		let c = 0;
		for (let i of this.array) if (i[1] > c) c = i[1];
		return c;
	}
	
	findOperand(operation) {
		for (let i of this.array) if (i[1] == operation) return this.array.indexOf(i);
		return -1;
	}
	
	standardize() {
		if (this.isNaN) {
			this.array = [[NaN, NaN]];
			this.sign = NaN;
			this.layer = NaN;
		} else if (!this.isFinite || this.layer > ExpandtaNum.MSI) {
			if (this.array[0][0] < 0) this.sign = -1;
			else this.sign = 1;
			this.array = [[Infinity, Infinity]];
			this.layer = Infinity;
		} else {
			for (let i of this.array) {
				if (i[0] == 0 && i[1] != 0) this.array.splice(this.array.indexOf(i), 1);
			}
			if (this.array.length == 0) {
				this.array = [[0, 0]];
				this.layer = 0;
				this.sign = 1;
			}
			if (this.layer == 0 && this.findOperand(0) > -1 && this.array[this.findOperand(0)][0] < 0) {
				this.array[this.findOperand(0)][0] *= -1;
				this.sign *= -1;
			}
			if (this.array.length == 1 && this.findOperand(0) > -1 && this.array[this.findOperand(0)][0] == 0) {
				this.array = [[0, 0]];
			}
			if (this.highest_operation > ExpandtaNum.MSI) {
				this.array = [[this.highest_operation, 0]];
				this.layer++;
			}
			if (this.array.length == 1 && this.array[0][0] < ExpandtaNum.MSI && this.layer > 0) {
				this.array = [[10, this.array[0][0]]];
				this.layer--;
			}
			let fo0 = this.findOperand(0);
			let fo1 = this.findOperand(1);
			if (fo0 > -1 && fo1 > -1) if (this.array[fo0][0] < ExpandtaNum.ME && this.array[fo1][0] > 0) {
				this.array[fo0][0] = Math.pow(10, this.array[fo0][0]);
				this.array[fo1][0]--;
			}
			for (let i of this.array) {
				if (i[0] > ExpandtaNum.MSI) {
					if (i[1] == 0) {
						let fo = this.findOperand(1);
						if (fo == -1) this.array.push([1, 1]);
						else this.array[fo][0]++;
						fo = this.findOperand(0);
						this.array[fo][0] = Math.log10(this.array[fo][0]);
					} else {
						let fo = this.findOperand(i[1] + 1);
						if (fo == -1) this.array.push([1, i[1] + 1]);
						else this.array[fo][0]++;
						for (let j of this.array) {
							if (j[1] < i[1] + 1) {
								if (j[1] == 0) {
									j[0] = i[0] + 1;
								} else {
									j[0] = 0;
								}
							}
						}
					}
				}
			}
			this.array = this.array.sort((x, y) => x[1] > y[1] ? 1 : -1);
		}
	}
}