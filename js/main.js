let app;

function init() {
	app = new Vue({
		el: '#app',
		data: {
			init: true,
			sidebar_state: false,
			tab: 0
		},
		methods: {
			toggle_sidebar: function() {this.sidebar_state = !this.sidebar_state},
			tab_to: function(n) {this.tab = n},
			save: function() {
				let str = btoa(JSON.stringify(this.$data));
				localStorage.setItem('infgsave', str);
			},
			load: function() {
				let sv = localStorage.getItem('infgsave');
				if (sv == null) return false;
				sv = JSON.parse(atob(sv));
				return true;
			}
		}
	});
	
	load();
}